/**
 * app.js
 *
 * This file contains some conventional defaults for working with Socket.io + Sails.
 * It is designed to get you up and running fast, but is by no means anything special.
 *
 * Feel free to change none, some, or ALL of this file to fit your needs!
 */
var bookArr=[
  {id:"genesis",label:"創世紀",chapters:50},
  {id:"exodus",label:"出埃及記",chapters:40},
  {id:"leviticus",label:"利未記",chapters:27},
  {id:"numbers",label:"民數記",chapters:36},
  {id:"deuteronomy",label:"申命記",chapters:34},
  {id:"joshua",label:"約書亞記",chapters:24},
  {id:"judges",label:"士師記",chapters:21},
  {id:"ruth",label:"路得記",chapters:4},
  {id:"samuel_1",label:"撒母耳記上",chapters:31},
  {id:"samuel_2",label:"撒母耳記下",chapters:24},
  {id:"kings_1",label:"列王記上",chapters:22},
  {id:"kings_2",label:"列王記下",chapters:25},
  {id:"chronicles_1",label:"歷代志上",chapters:29},
  {id:"chronicles_2",label:"歷代志上",chapters:36},
  {id:"ezra",label:"以斯拉記",chapters:10},
  {id:"nehemiah",label:"尼希米記",chapters:13},
  {id:"esther",label:"以斯帖記",chapters:10},
  {id:"job",label:"約伯記",chapters:42},
  {id:"psalms",label:"詩篇",chapters:150},
  {id:"proverbs",label:"箴言",chapters:31},
  {id:"ecclesiastes",label:"傳道書",chapters:12},
  {id:"song_of_songs",label:"雅歌",chapters:8},
  {id:"isaiah",label:"以賽亞書",chapters:66},
  {id:"jeremiah",label:"耶利米書",chapters:52},
  {id:"lamentation",label:"耶利米哀歌",chapters:5},
  {id:"ezekiel",label:"以西結書",chapters:48},
  {id:"daniel",label:"但以理書",chapters:12},
  {id:"hosea",label:"何西阿書",chapters:14},
  {id:"joel",label:"約珥書",chapters:3},
  {id:"amos",label:"阿摩司書",chapters:9},
  {id:"obadiah",label:"俄巴底亞書",chapters:1},
  {id:"jonah",label:"約拿書",chapters:4},
  {id:"micah",label:"彌迦書",chapters:7},
  {id:"nahum",label:"那鴻書",chapters:3},
  {id:"habakkuk",label:"哈巴谷書",chapters:3},
  {id:"zephaniah",label:"西番雅書",chapters:3},
  {id:"haggai",label:"哈該書",chapters:2},
  {id:"zechariah",label:"撒迦利亞書",chapters:14},
  {id:"malachi",label:"瑪拉基書",chapters:4},
  {id:"matthew",label:"馬太福音",chapters:28},
  {id:"mark",label:"馬可福音",chapters:16},
  {id:"luke",label:"路加福音",chapters:24},
  {id:"john",label:"約翰福音",chapters:21},
  {id:"acts",label:"使徒行傳",chapters:28},
  {id:"romans",label:"羅馬書",chapters:16},
  {id:"corinthians_1",label:"哥林多前書",chapters:16},
  {id:"corinthians_2",label:"哥林多後書",chapters:13},
  {id:"galatians",label:"加拉太書",chapters:6},
  {id:"ephesians",label:"以弗所書",chapters:6},
  {id:"philippians",label:"腓立比書",chapters:4},
  {id:"colossians",label:"歌羅西書",chapters:4},
  {id:"thessalonians_1",label:"帖撒羅尼迦前書",chapters:5},
  {id:"thessalonians_2",label:"帖撒羅尼迦後書",chapters:3},
  {id:"timothy_1",label:"提摩太前書",chapters:6},
  {id:"timothy_2",label:"提摩太後書",chapters:4},
  {id:"titus",label:"提多書",chapters:3},
  {id:"philemon",label:"腓利門書",chapters:1},
  {id:"hebrews",label:"希伯來書",chapters:13},
  {id:"james",label:"雅各書",chapters:5},
  {id:"peter_1",label:"彼得前書",chapters:5},
  {id:"peter_2",label:"彼得後書",chapters:3},
  {id:"john_1",label:"約翰一書",chapters:5},
  {id:"john_2",label:"約翰二書",chapters:1},
  {id:"john_3",label:"約翰三書",chapters:1},
  {id:"jude",label:"猶大書",chapters:1},
  {id:"revelation",label:"啟示錄",chapters:22}
];

;(function () {

  angular.module('app',['ngResource'])

  .factory('apiService',['$resource',function($resource){
    this.Record = $resource('/record/:id',{id:'@id'},{
      update:{method:'put'}
    });

    return this;
  }])

  //Controllers
  .controller('CalendarCtrl',['$scope','$element','$compile','apiService','$filter',function($scope,$element,$compile,apiService,$filter){
    $($element).fullCalendar({
      theme:'true',
      header:{
        left:'',
        center:'title',
        right:'prev,next,today'
      },
      allDayDefault:true,
      dayClick:function(date,allDay,e,view){
        var childScope = $scope.showRecordDialog();
        childScope.$broadcast('set date',date);
      },
      eventClick:function(event,e,view){
        //$('#record-modal').foundation('reveal','open')
        var childScope = $scope.showRecordDialog();
        childScope.$broadcast('set event',event);
      },
      events:function(start,end,callback){
        var start = $filter('date')(start,'yyyy-MM-dd');
        var end = $filter('date')(end,'yyyy-MM-dd')
        apiService.Record.query({start:start,end:end},function(data){
          //var events = [];
          // angular.forEach(data,function(value,key){
          //   this[key] = value;
          //   this[key].title = this[key].
          // },events)

          callback(data)
        })
      },
      eventRender:function(event,element){
        var book = _.find(bookArr,function(book){return book.id == event.book});


        element.html(book.label + event.chapter + '章')
      }
    })

    

    $scope.showRecordDialog=function(date){
      var recordCompile = $compile($('.record-modal').clone())
      var childScope = $scope.$new();
      var dialog = recordCompile(childScope);


      $(dialog).foundation('reveal','open');
      return childScope;
    }
    $scope.closeRecordDialog=function(){
      $('.record-modal').foundation('reveal','close');
    }

    $scope.$on('refreshCalendar',function(e){
      e.stopPropagation();
      $($element).fullCalendar('refetchEvents');
    })



  }])

  .controller('RecordCtrl',['$scope','$filter','apiService',function($scope,$filter,apiService){
    var Record = apiService.Record;

    $scope.record=new Record();
    $scope.bookArr = bookArr;

    //Generate available chapters for options
    $scope.$watch('record.book',function(newVal,oldVal){
      if(!newVal) return;
      var book = _.find(bookArr,function(book){return book.id == newVal});
      var chapters = [];
      for(var i=1;i<=book.chapters;++i){
        chapters.push({label:i,value:i});
      }

      $scope.validChapters=chapters;
      if($scope.record.chapter > book.chapters){
        $scope.record.chapter = null;
      }
    })

    $scope.setDate = function(date){
      var date = $filter('date')(date,'yyyy-MM-dd');

      var record = Record.query({date:date},function(){

        if(record.length){
          $scope.record = record[0];
        }else{
          $scope.record=new Record();
          $scope.record.date = date;
        }

      });
      
    }

    $scope.$on('set date',function(e,date){
      $scope.setDate(date);
      $scope.$apply(function(){});
    })
    $scope.$on('set event',function(e,data){
      $scope.record = Record.get({id:data.id});
    })

    $scope.save=function(){
      //var record = new Record($scope.record);
      var callback = function(){
        console.log('saved');
        $scope.$emit('refreshCalendar');
        $scope.$parent.closeRecordDialog();
      };


      if($scope.record.id){
        $scope.record.$update(callback)
      }else{
        $scope.record.$save(callback)
      }
      
    }


  }])



  
















  //Directive and Filter Helpers
  .directive('nsEditorHtml', function ($log,$timeout) {
    return {
      restrict: "CA",
      replace: true,
      require: "^ngModel",
      scope : true,
      link: function (scope, ele, attrs, ngModel) {
        if(scope.ckEditor) return;
        scope.ckEditor = CKEDITOR.replace(ele[0], {
           extraPlugins: "onchange"
        });

        scope.ckEditor.on('change', function (ev) {
          try {
            scope.$apply(function () {
              ngModel.$setViewValue(ev.editor.getData())
            });
          } catch (e) {

          }
        });

        ngModel.$render = function () {
          scope.ckEditor.setData(ngModel.$viewValue);
        };
      }
    }
  })
  
  .filter('range', function () {
    return function (input,low,high) {
        var lowBound = high ? low : 1,
            highBound = high ? high : low;

        var result = [];
        for (var i = lowBound; i <= highBound; i++)
            result.push(i);
        return result;
      };
  })































  angular.bootstrap(document,['app']);
})();
