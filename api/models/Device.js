/**
 * Device
 *
 * @module      :: Model
 * @description :: A short summary of how this model works and what it represents.
 * @docs		:: http://sailsjs.org/#!documentation/models
 */

module.exports = {

  attributes: {
  	token:'string',
  	type:'string',
  	subscribe:'boolean'
  	/* e.g.
  	nickname: 'string'
  	*/
    
  }

};
