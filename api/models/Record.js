/**
 * Record
 *
 * @module      :: Model
 * @description :: A short summary of how this model works and what it represents.
 * @docs		:: http://sailsjs.org/#!documentation/models
 */

module.exports = {

  attributes: {
  	
  	/* e.g.
  	nickname: 'string'
  	*/
    title:'string',
    date:{
      type:'date',
      required:true
    },
  	book:{
  		type:'string',
  		required:true
  	},
  	chapter:{
  		type:'integer',
  		required:true
  	},
    speaker:'string',
  	audio:'url',
  	content:'text'
    
  }

};
