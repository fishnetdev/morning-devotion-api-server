/**
 * RecordController
 *
 * @module      :: Controller
 * @description	:: A set of functions called `actions`.
 *
 *                 Actions contain code telling Sails how to respond to a certain type of request.
 *                 (i.e. do stuff, then send some JSON, show an HTML page, or redirect to another URL)
 *
 *                 You can configure the blueprint URLs which trigger these actions (`config/controllers.js`)
 *                 and/or override them with custom routes (`config/routes.js`)
 *
 *                 NOTE: The code you write here supports both HTTP and Socket.io automatically.
 *
 * @docs        :: http://sailsjs.org/#!documentation/controllers
 */

module.exports = {
    
  


  /**
   * Overrides for the settings in `config/controllers.js`
   * (specific to RecordController)
   */
  _config: {},

  find:function(req,res){
  	if(req.params.id){
  		Record.findOne(req.params.id).done(function(err,record){
	  		if(err) return res.json(err);
	  		res.json(record);
	  	})
	  	return;
  	}

  	if(req.query.start){
      req.query.date = req.query.date || {}
      req.query.date['>=']=req.query.start != "today" ? req.query.start : new Date();
      delete req.query.start;
    }

  	if(req.query.end){
      req.query.date = req.query.date || {}
      req.query.date['<=']=req.query.end != "today" ? req.query.end : new Date();
      delete req.query.end;
    }

  	Record.find(req.query).sort('date ASC').done(function(err,records){
  		if(err) return res.json(err);
	  	res.json(records);
  	})

  },

  create:function(req,res){
  	Record.create(req.body).done(function(err,record){
  		if(err) return res.json(err);
  		res.json(record);
  	});
  },

  update:function(req,res){
  	Record.update(req.body.id,req.body,function(err, records){
  		if(err) return res.json(err);
  		res.json(records[0]);
  	})
  },

  destroy:function(req,res){
  	Record.destroy(req.params.id)
  },

  //Will Deprecate after migration
  Synchronize:function(req,res){

    var formatDate=function(d){
      if(!d) return '';
      var month = d.getMonth()+1;
      var date = d.getDate()+1;

      return d.getFullYear()+'-'+(month<10?"0"+month:month)+'-'+(date<10?"0"+date:date);
    }

    var formatDateTime=function(d){
      if(!d) return '';
      var hours = d.getHours();
      var minutes = d.getMinutes();
      var seconds = d.getSeconds();

      return formatDate(d) + ' '+(hours<10?"0"+hours:hours)+':'+(minutes<10?"0"+minutes:minutes)+':'+(seconds<10?"0"+seconds:seconds);
    }



    var locale = req.query.locale;
    var lastSynDate = new Date(req.query.lastSynDate);

    Record.find({
      updatedAt:{'>=':lastSynDate}
    }).sort('date ASC').done(function(err,records){
      if(err) return res.json(err);
      //console.log(records);

      var result = [];
      for(var i in records){
        var record = records[i];
        record.pastor = record.speaker;
        delete record.speaker;

        if(record.content==null) delete record.content;
        
        record.action = "UPDATE";

        console.log("record.createdAt: %s\nrecord.updatedAt: %s\nlastSynDate     : %s\n",new Date(record.createdAt),record.updatedAt,lastSynDate)
        if(lastSynDate < new Date(record.createdAt)){
          record.action = "INSERT";
          console.log("set to INSERT");
        }
        
        var tempDatetime = record.updatedAt;
        tempDatetime.setHours(record.updatedAt.getHours()-8);

        record.update_date = formatDateTime(tempDatetime);
        result.push({
          id:record.id,
          data:record
        })
              
      }

      res.json({
        row:result,
        datetime:formatDateTime(new Date())
      });
    })

  }

  
};
