/**
 * DeviceController
 *
 * @module      :: Controller
 * @description	:: A set of functions called `actions`.
 *
 *                 Actions contain code telling Sails how to respond to a certain type of request.
 *                 (i.e. do stuff, then send some JSON, show an HTML page, or redirect to another URL)
 *
 *                 You can configure the blueprint URLs which trigger these actions (`config/controllers.js`)
 *                 and/or override them with custom routes (`config/routes.js`)
 *
 *                 NOTE: The code you write here supports both HTTP and Socket.io automatically.
 *
 * @docs        :: http://sailsjs.org/#!documentation/controllers
 */
var _  = require('underscore');

module.exports = {
    
  subscribe:function(req,res){
    var data = _.defaults({
      subscribe:true
    }, req.method=="POST" ? req.body : req.query);


    Device.findOneByToken(data.token).done(function(err,device){

      var callback = function(err,device){
        if(err) return res.json(err);
        console.log(device);
        res.json(device);
      };

      if(device){
        //Device already exist
        console.log('Update existing device');
        Device.update({token:device.token},{subscribe:true}).done(callback);
      }else{
        //Create new device
        console.log('Create new device');
        Device.create(data).done(callback);
      }
      
    });
    
  },
  unsubscribe:function(req,res){
    var token = req.query.token;

    Device.update({token:token},{subscribe:false}).done(function(err,devices){
      if(err) return res.json(err);
      console.log('Device %s Unsubscribed',devices[0].token);
      console.log(devices);
      res.json(devices);
    })

  },
  send:function(req,res){

  },



  /**
   * Overrides for the settings in `config/controllers.js`
   * (specific to DeviceController)
   */
  _config: {}

  
};
